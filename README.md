# LE40 SpotWelder

3D models, hardware design & software implementation

Project developed in [JetBrains CLion](https://www.jetbrains.com/clion/ "JetBrains CLion") IDE

Arduino support with [lecho-arduino-clion](https://github.com/olegrak/lecho-arduino-clion "lecho-arduino-clion") build scripts.

![](Misc/images/6.jpg)

Assembly order:

![](Misc/images/1.jpg)
![](Misc/images/2.jpg)
![](Misc/images/3.jpg)
![](Misc/images/4.jpg)
![](Misc/images/5.jpg)
![](Misc/images/6.jpg)
