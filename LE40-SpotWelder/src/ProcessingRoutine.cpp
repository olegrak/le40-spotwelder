#include <HID.h>
#include "Constants.h"
#include "ProcessingRoutine.h"

ProcessingRoutine::ProcessingRoutine(Settings *settings, void (*onBtnClicked)(void)) {
    this->settings = settings;
    this->onBtnClicked = onBtnClicked;
}

void ProcessingRoutine::init() {
    pinMode(PIN_TPOT, INPUT);
    pinMode(PIN_CPOT, INPUT);
    pinMode(PIN_BTN, INPUT_PULLUP);
    pinMode(PIN_OUT, OUTPUT);

    this->attachBtnClickHandler();
}

void ProcessingRoutine::startExecution() {
    detachInterrupt(uint8_t digitalPinToInterrupt(PIN_BTN));

    impulseTime = (settings->impulsePeriodsCount - 1) * LINE_PERIOD_MILLIS + LINE_SAFETY_BUFFER_MILLIS;
    impulsesLeft = settings->impulsesCount;

    btnClicked = false;
}

void ProcessingRoutine::stopExecution() {
    delay(SHOT_POST_DELAY);
    this->attachBtnClickHandler();
}

bool ProcessingRoutine::hasToContinueExecution() {
    return 0 < impulsesLeft;
}

void ProcessingRoutine::attachBtnClickHandler() {
    attachInterrupt(
            uint8_t digitalPinToInterrupt(PIN_BTN),
            this->onBtnClicked,
            LOW);
}
