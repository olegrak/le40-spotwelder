#ifndef CLIONARDUINO_PROCESSINGDATA_H
#define CLIONARDUINO_PROCESSINGDATA_H

#include "Settings.h"

class ProcessingRoutine {
public:
    volatile bool btnClicked;
    unsigned long impulseTime;
    int impulsesLeft;

    ProcessingRoutine(Settings*, void (*)(void));

    void init();

    void startExecution();

    void stopExecution();

    bool hasToContinueExecution();

private:
    Settings* settings;
    void (*onBtnClicked)(void);

    void attachBtnClickHandler(void);
};


#endif //CLIONARDUINO_PROCESSINGDATA_H
