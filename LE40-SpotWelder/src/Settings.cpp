#include <HID.h>
#include "Settings.h"

Settings::Settings()
{
    this->impulsesCount = 1;
    this->impulsePeriodsCount = 1;

    this->countPotValue = -1;
    this->timePotValue = -1;
}

bool Settings::check()
{
    //I need to shift reading, so the max value is 1024, but not 1023
    int time = 1 + analogRead(PIN_TPOT);
    int count = 1 + analogRead(PIN_CPOT);

    if (time == timePotValue && count == countPotValue)
        return false;

    impulsePeriodsCount =
        MIN_PERIODS_IN_IMPULSE +
        (int)(1.0f * (MAX_PERIODS_IN_IMPULSE - MIN_PERIODS_IN_IMPULSE) * time / ANALOG_READ_MAX);
    timePotValue = time;

    impulsesCount =
        MIN_IMPULSE_COUNT +
        (int)(1.0f * (MAX_IMPULSE_COUNT - MIN_IMPULSE_COUNT) * count / ANALOG_READ_MAX);
    countPotValue = count;

    return true;
}