#ifndef CLIONARDUINO_CONSTANTS_H
#define CLIONARDUINO_CONSTANTS_H

#define LCD_I2CADDR 0x27 // Proteus
//#define LCD_I2CADDR 0x3F // Real Device
#define LCD_DATA_START_COL 12

#define ANALOG_READ_MAX 1024

#define PIN_TPOT A1
#define PIN_CPOT A0
#define PIN_BTN 2
#define PIN_OUT 5

#define LINE_FREQUENCY 50           //50Hz for Ukraine, so full period is 1s/50Hz=0.02sec
#define LINE_PERIOD_MILLIS (unsigned) (1000 / LINE_FREQUENCY)
#define LINE_SAFETY_BUFFER_MILLIS (unsigned) 1 //We need this one to shoot correct count of impulses. TRIAC related

#define MIN_PERIODS_IN_IMPULSE 5
#define MAX_PERIODS_IN_IMPULSE 50
#define MIN_IMPULSE_COUNT 1
#define MAX_IMPULSE_COUNT 5

#define SHOT_POST_DELAY 500

#endif //CLIONARDUINO_CONSTANTS_H
