#ifndef CLIONARDUINO_LCD_H
#define CLIONARDUINO_LCD_H

#include <LiquidCrystal_I2C.h>
#include "Settings.h"

class LCD {
public:
    LCD();

    void printSettingsTemplate();
    void printSettings(Settings* settings);

private:
    LiquidCrystal_I2C* lcd;
};


#endif //CLIONARDUINO_LCD_H
