#include <stdio.h>
#include "Constants.h"
#include "LCD.h"

LCD::LCD() {
    lcd = new LiquidCrystal_I2C(LCD_I2CADDR, 16, 2);
    lcd->init();
    lcd->clear();
    lcd->backlight();
}

void LCD::printSettingsTemplate() {
    lcd->setCursor(0, 0);
    lcd->print("Time:");
    lcd->setCursor(0, 1);
    lcd->print("Count:");
}

void LCD::printSettings(Settings *settings) {
    unsigned int time = LINE_PERIOD_MILLIS * settings->impulsePeriodsCount;
    char buffer[4];
    sprintf(buffer, "%4i", time);
    lcd->setCursor(LCD_DATA_START_COL, 0);
    lcd->print(buffer);

    lcd->setCursor(LCD_DATA_START_COL + 3, 1);
    lcd->print(settings->impulsesCount);
}
