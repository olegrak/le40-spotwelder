#ifndef CLIONARDUINO_SETTINGS_H
#define CLIONARDUINO_SETTINGS_H

#include "Constants.h"

class Settings {
public:
    int impulsePeriodsCount;
    int impulsesCount;

    Settings();

    bool check();

private:
    int timePotValue;
    int countPotValue;
};


#endif //CLIONARDUINO_SETTINGS_H
