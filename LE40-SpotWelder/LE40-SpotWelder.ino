#include <Arduino.h>
#include "src/Constants.h"
#include "src/Settings.h"
#include "src/ProcessingRoutine.h"
#include "src/LCD.h"

Settings* settings;
ProcessingRoutine* routine;
LCD* lcd;

#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
void setup()
{
    settings = new Settings();

    routine = new ProcessingRoutine(settings, onBtnClicked);
    routine->init();

    lcd = new LCD();
    lcd->printSettingsTemplate();
    lcd->printSettings(settings);
}

#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
void loop()
{
    if (routine->btnClicked) {
        routine->startExecution();
        do {
            routine->impulsesLeft--;

            digitalWrite(PIN_OUT, HIGH);
            digitalWrite(LED_BUILTIN, HIGH);
            delay(routine->impulseTime);
            digitalWrite(PIN_OUT, LOW);
            digitalWrite(LED_BUILTIN, LOW);

            if (routine->hasToContinueExecution())
                delay(routine->impulseTime);

        } while (routine->hasToContinueExecution());
        routine->stopExecution();
    }

    if (settings->check())
        lcd->printSettings(settings);

    delay(LINE_PERIOD_MILLIS);
}

void onBtnClicked() {
    routine->btnClicked = true;
}